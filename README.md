# Text-Em-All Coding Challenge

This repository contains a C# implementation of the API requested by the [Text-Em-All Back End Coding Challenge](https://github.com/callemall/tea-c-sharp-challenge). The solution was developed with Visual Studio 2019 and targets .NET Core 3.1.

## Project structure

- `src/` contains the solution and all project code.
- `scripts/` contains SQL scripts required by the challenge.

## Configuration

The database connection string is configured in `src/TEAChallenge.Api/appsettings.json`. Please modify this value as needed for your particular environment.

## Endpoints

- `GET /students`: Gets all students and their GPAs where available. (This implementation returns *all* students, including those who are enrolled but have not received a grade, as well as those who are not enrolled in any courses at all. Students with no grades are returned with a GPA of `0`.)
- `GET /students/{studentId}/transcript`: Gets a single student's information and transcript for all courses for which the student has received a grade.
- `POST /grades`: Inserts (or replaces) a student's grade for a particular course. (See the comments in `src/TEAChallenge.Services/SqlEnrollmentService.cs` for the reasoning behind allowing multiple calls.)