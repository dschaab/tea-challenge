﻿using System;
using System.Runtime.Serialization;

namespace TEAChallenge.Core
{
    public sealed class DataMalformedException : Exception
    {
        public DataMalformedException()
            : this("The given data is malformed.")
        {
        }

        public DataMalformedException(string message) : base(message)
        {
        }

        public DataMalformedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public DataMalformedException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
