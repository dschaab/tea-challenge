﻿using System.Collections.Generic;

namespace TEAChallenge.Core
{
    public sealed class StudentTranscript
    {
        public int StudentId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public decimal Gpa { get; set; }

        public IList<CourseGrade> Grades { get; set; }

        public sealed class CourseGrade
        {
            public int CourseId { get; set; }

            public string Title { get; set; }

            public int Credits { get; set; }

            public decimal Grade { get; set; }
        }
    }
}
