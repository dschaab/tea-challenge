﻿namespace TEAChallenge.Core
{
    public sealed class Student
    {
        public int StudentId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public decimal Gpa { get; set; }
    }
}
