﻿using System;
using System.Data;

namespace TEAChallenge.Core
{
    /// <summary>
    ///   Represents a connection to a particular database. An implementation
    ///   should exist for every database engine supported by the project.
    /// </summary>
    public interface IDatabase
    {
        /// <summary>
        ///   Gets a value indicating whether the database is accessible and
        ///   responding to queries in a reasonable way.
        /// </summary>
        bool IsHealthy { get; }

        /// <summary>
        ///   Executes the given command.
        /// </summary>
        /// <param name="commandText">
        ///   The SQL command text.
        /// </param>
        /// <param name="parameters">
        ///   A set of optional parameters.
        /// </param>
        void Execute(string commandText, object parameters = null);

        /// <summary>
        ///   Executes the given command and returns a single value.
        /// </summary>
        /// <typeparam name="T">
        ///   The data type expected to be returned by the command.
        /// </typeparam>
        /// <param name="commandText">
        ///   The SQL command text.
        /// </param>
        /// <param name="parameters">
        ///   A set of optional parameters.
        /// </param>
        /// <returns>
        ///   The scalar value.
        /// </returns>
        T ReadScalar<T>(string commandText, object parameters = null);

        /// <summary>
        ///   Executes the given command and, for each row returned, executes
        ///   the given action.
        /// </summary>
        /// <param name="commandText">
        ///   The SQL command text.
        /// </param>
        /// <param name="parameters">
        ///   A set of optional parameters.
        /// </param>
        /// <param name="action">
        ///   An action to execute for each row of the data returned by the
        ///   command.
        /// </param>
        void ReadAll(string commandText, object parameters = null, Action<IDataReader> action = null);
    }
}
