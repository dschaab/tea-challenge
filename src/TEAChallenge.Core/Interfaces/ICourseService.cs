﻿namespace TEAChallenge.Core
{
    public interface ICourseService
    {
        /// <summary>
        ///   Determines whether a course exists.
        /// </summary>
        /// <param name="courseId">
        ///   The course's unique ID.
        /// </param>
        /// <returns><c>true</c> if the course exists.</returns>
        bool CourseExists(int courseId);
    }
}
