﻿using System.Collections.Generic;

namespace TEAChallenge.Core
{
    /// <summary>
    ///   Describes an interface for requesting student data from some backing
    ///   data store.
    /// </summary>
    public interface IStudentService
    {
        /// <summary>
        ///   Determines whether a student exists.
        /// </summary>
        /// <param name="studentId">
        ///   The student's unique ID.
        /// </param>
        /// <returns><c>true</c> if the student exists.</returns>
        bool StudentExists(int studentId);

        /// <summary>
        ///   Gets all students and their respective GPAs (if available).
        /// </summary>
        /// <returns>
        ///   A list of students.
        /// </returns>
        IList<Student> GetStudents();

        /// <summary>
        ///   Gets a student's transcript, consisting of all classes for which
        ///   the student has attained a grade.
        /// </summary>
        /// <param name="studentId">
        ///   The student's unique ID.
        /// </param>
        /// <returns>
        ///   A transcript, or <c>null</c> if the student does not exist.
        /// </returns>
        StudentTranscript GetTranscript(int studentId);
    }
}
