﻿namespace TEAChallenge.Core
{
    public interface IEnrollmentService
    {
        /// <summary>
        ///   Enrolls a student in a course.
        /// </summary>
        /// <param name="studentId">
        ///   The student's unique ID.
        /// </param>
        /// <param name="courseId">
        ///   The course's unique ID.
        /// </param>
        /// <param name="grade">
        ///   The grade, which may be between 0 and 4, inclusive, or
        ///   <c>null</c> if the student has not yet received a grade.
        /// </param>
        /// <returns>
        ///   The enrollment data.
        /// </returns>
        Enrollment EnrollStudent(int studentId, int courseId, decimal? grade);
    }
}
