﻿using Microsoft.AspNetCore.Mvc;

using TEAChallenge.Core;

namespace TEAChallenge.Api.Controllers
{
    [ApiController]
    [Route("/")]
    public class RootController : ControllerBase
    {
        private readonly IDatabase _database;

        public RootController(
            IDatabase database)
        {
            _database = database;
        }

        [HttpGet]
        public object Get()
        {
            return new
            {
                DatabaseUp = _database.IsHealthy,
            };
        }
    }
}
