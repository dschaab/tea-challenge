﻿using Microsoft.AspNetCore.Mvc;

using TEAChallenge.Core;

namespace TEAChallenge.Api.Controllers
{
    [ApiController]
    public class EnrollmentsController : ControllerBase
    {
        private readonly IEnrollmentService _service;

        public EnrollmentsController(
            IEnrollmentService service)
        {
            _service = service;
        }

        [HttpPost]
        [Route("grades")]
        public IActionResult PostGrade(
            [FromBody] Enrollment enrollment)
        {
            try
            {
                enrollment = _service.EnrollStudent(
                    enrollment.StudentId,
                    enrollment.CourseId,
                    enrollment.Grade);
                return Ok(new
                {
                    GradeId = enrollment.EnrollmentId,
                    enrollment.StudentId,
                    enrollment.CourseId,
                    enrollment.Grade,
                });
            }
            catch (DataMalformedException dme)
            {
                // Normally we wouldn't expose the exception message directly
                // to the user in case the message reveals sensitive
                // information, but in this case we know the set of possible
                // messages and can safely send them.
                return UnprocessableEntity(new
                {
                    Code = "MALFORMED",
                    Message = dme.Message,
                    CanRetry = false,
                });
            }
        }
    }
}
