﻿using Microsoft.AspNetCore.Mvc;

using TEAChallenge.Core;

namespace TEAChallenge.Api.Controllers
{
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly IStudentService _service;

        public StudentsController(
            IStudentService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("students")]
        public IActionResult GetStudents()
        {
            return Ok(_service.GetStudents());
        }

        [HttpGet]
        [Route("students/{studentId}/transcript")]
        public IActionResult GetStudentTranscript(int studentId)
        {
            var transcript = _service.GetTranscript(studentId);
            if (transcript == null)
            {
                return NotFound(new
                {
                    Code = "NOTFOUND",
                    Message = "There is no student with the given ID.",
                    CanRetry = false,
                });
            }
            return Ok(transcript);
        }
    }
}
