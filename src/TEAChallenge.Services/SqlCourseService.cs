﻿using System;

using TEAChallenge.Core;

namespace TEAChallenge.Services
{
    public sealed class SqlCourseService : ICourseService
    {
        private readonly IDatabase _database;

        public SqlCourseService(
            IDatabase database)
        {
            _database = database ?? throw new ArgumentNullException(nameof(database));
        }

        public bool CourseExists(int courseId)
        {
            var commandText = "SELECT * FROM Course WHERE CourseID = @CourseID";
            var parameters = new { CourseID = courseId };
            var exists = false;

            _database.ReadAll(commandText, parameters, _ =>
            {
                exists = true;
            });

            return exists;
        }
    }
}
