﻿using System;

using TEAChallenge.Core;

namespace TEAChallenge.Services
{
    /// <summary>
    ///   Provides an implementation of EnrollmentService that reads data
    ///   stored in a SQL database.
    /// </summary>
    public sealed class SqlEnrollmentService : IEnrollmentService
    {
        private readonly IStudentService _studentService;
        private readonly ICourseService _courseService;
        private readonly IDatabase _database;

        public SqlEnrollmentService(
            IStudentService studentService,
            ICourseService courseService,
            IDatabase database)
        {
            _studentService = studentService ?? throw new ArgumentNullException(nameof(studentService));
            _courseService = courseService ?? throw new ArgumentNullException(nameof(courseService));
            _database = database ?? throw new ArgumentNullException(nameof(database));
        }

        public Enrollment EnrollStudent(int studentId, int courseId, decimal? grade)
        {
            // Take care of data validation here rather than depending on the
            // database's constraints (which may not exist) or logic duplicated
            // at higher layers (which may differ).
            if (!_studentService.StudentExists(studentId))
            {
                throw new DataMalformedException("There is no student with the given ID.");
            }
            if (!_courseService.CourseExists(courseId))
            {
                throw new DataMalformedException("There is no course with the given ID.");
            }
            if (grade.HasValue && (grade < 0 || grade > 4))
            {
                throw new DataMalformedException("Grade must either be null or be between 0.00 and 4.00 inclusive.");
            }

            // This method assumes that it is permissible to call more than
            // once. (For example, once when the student is first enrolled in a
            // course and once when the student's final grade is posted.) We
            // could come up with an upsert command, but we'll keep things
            // simple for now and look for an existing enrollment first and
            // then decide what to do based on its existence.
            //
            // We can get away with this because this is a trivial project. A
            // real-world project would probably also have a method to drop a
            // student from a course, and if that method were called while this
            // method is executing, we might throw an exception when attempting
            // to update an enrollment that no longer exists.
            //
            // We may also want to make this method less chatty overall by
            // trying to update the database and return the desired data in a
            // single command.
            var enrollment = GetEnrollment(studentId, courseId);

            string commandText;
            if (enrollment == null)
            {
                commandText = @"
                    INSERT INTO StudentGrade (StudentID, CourseID, Grade)
                    VALUES (@StudentID, @CourseID, @Grade)";
            }
            else
            {
                commandText = @"
                    UPDATE StudentGrade
                    SET Grade = @Grade
                    WHERE StudentID = @StudentID AND CourseID = @CourseID";
            }

            var parameters = new
            {
                StudentID = studentId,
                CourseID = courseId,
                Grade = grade,
            };

            _database.Execute(commandText, parameters);

            enrollment = GetEnrollment(studentId, courseId);
            return enrollment;
        }

        private Enrollment GetEnrollment(int studentId, int courseId)
        {
            var commandText = @"
                SELECT EnrollmentID, StudentID, CourseID, Grade
                FROM StudentGrade
                WHERE StudentID = @StudentID AND CourseID = @CourseID";
            var parameters = new
            {
                StudentID = studentId,
                CourseID = courseId,
            };
            var enrollment = null as Enrollment;

            _database.ReadAll(commandText, parameters, action: reader =>
            {
                enrollment = new Enrollment
                {
                    EnrollmentId = reader.GetInt32(reader.GetOrdinal("EnrollmentID")),
                    StudentId = reader.GetInt32(reader.GetOrdinal("StudentID")),
                    CourseId = reader.GetInt32(reader.GetOrdinal("CourseID")),
                };
                if (!reader.IsDBNull(reader.GetOrdinal("Grade")))
                {
                    enrollment.Grade = reader.GetDecimal(reader.GetOrdinal("Grade"));
                }
            });

            return enrollment;
        }
    }
}
