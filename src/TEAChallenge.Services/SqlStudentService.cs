﻿using System;
using System.Collections.Generic;

using TEAChallenge.Core;

namespace TEAChallenge.Services
{
    /// <summary>
    ///   Provides an implementation of StudentService that reads data stored
    ///   in a SQL database. No assumptions are made as to the underlying
    ///   database engine.
    /// </summary>
    public class SqlStudentService : IStudentService
    {
        private readonly IDatabase _database;

        public SqlStudentService(
            IDatabase database)
        {
            _database = database ?? throw new ArgumentNullException(nameof(database));
        }

        public bool StudentExists(int studentId)
        {
            var commandText = @"
                SELECT *
                FROM Person
                WHERE PersonID = @PersonID AND Discriminator = 'Student'";
            var parameters = new { PersonID = studentId };
            var exists = false;

            _database.ReadAll(commandText, parameters, _ =>
            {
                exists = true;
            });

            return exists;
        }

        public IList<Student> GetStudents()
        {
            // Calculating each student's GPA requires access to the full
            // transcript. The average student probably has a small number of
            // grades, so transferring the data from the database and doing the
            // calculation on the API side is probably not that bad. If we were
            // to do this for a large school with thousands or tens of
            // thousands of students, however, we may want to perform these
            // calculations on the database server side as below. (Naturally we
            // should take into consideration the available hardware and do
            // proper profiling before prematurely deciding where to optimize.
            // It may be that network bandwidth is cheap and the API servers
            // have cycles to spare, so we would rather run calculations on the
            // API side.)
            var commandText = @"
                SELECT
                    p.PersonID,
                    p.FirstName,
                    p.LastName,
                    CASE WHEN gpa.GPA IS NULL THEN 0 ELSE gpa.GPA END AS GPA
                FROM
                    Person p
                    LEFT JOIN (
                        SELECT
                            sg.StudentID,
                            SUM(sg.Grade * c.Credits) / SUM(c.Credits) AS GPA
                        FROM
                            StudentGrade sg
                            INNER JOIN Course c ON sg.CourseID = c.CourseID
                        WHERE
                            sg.Grade IS NOT NULL
                            AND sg.Grade >= 0
                            AND c.Credits > 0
                        GROUP BY
                            sg.StudentID) gpa ON p.PersonID = gpa.StudentID
                WHERE
                    p.Discriminator = 'Student'";
            var students = new List<Student>();

            _database.ReadAll(commandText, action: reader =>
            {
                students.Add(new Student
                {
                    StudentId = reader.GetInt32(reader.GetOrdinal("PersonID")),
                    FirstName = reader.GetString(reader.GetOrdinal("FirstName")),
                    LastName = reader.GetString(reader.GetOrdinal("LastName")),
                    Gpa = reader.GetDecimal(reader.GetOrdinal("GPA")),
                });
            });

            return students;
        }

        public StudentTranscript GetTranscript(int studentId)
        {
            var commandText = @"
                SELECT
                    p.PersonID,
                    p.FirstName,
                    p.LastName,
                    CASE WHEN gpa.GPA IS NULL THEN 0 ELSE gpa.GPA END AS GPA,
                    c.CourseID,
                    c.Title,
                    c.Credits,
                    sg.Grade
                FROM
                    Person p
                    LEFT JOIN StudentGrade sg ON p.PersonID = sg.StudentID
                    INNER JOIN Course c ON sg.CourseID = c.CourseID
                    LEFT JOIN (
                        SELECT
                            sg.StudentID,
                            SUM(sg.Grade * c.Credits) / SUM(c.Credits) AS GPA
                        FROM
                            StudentGrade sg
                            INNER JOIN Course c ON sg.CourseID = c.CourseID
                        WHERE
                            sg.Grade IS NOT NULL
                            AND sg.Grade >= 0
                            AND c.Credits > 0
                        GROUP BY
                            sg.StudentID) gpa ON p.PersonID = gpa.StudentID
                WHERE
                    p.Discriminator = 'Student'
                    AND p.PersonID = @PersonID";
            var parameters = new
            {
                PersonID = studentId,
            };
            var transcript = null as StudentTranscript;

            _database.ReadAll(commandText, parameters, action: reader =>
            {
                transcript ??= new StudentTranscript
                {
                    StudentId = reader.GetInt32(reader.GetOrdinal("PersonID")),
                    FirstName = reader.GetString(reader.GetOrdinal("FirstName")),
                    LastName = reader.GetString(reader.GetOrdinal("LastName")),
                    Gpa = reader.GetDecimal(reader.GetOrdinal("GPA")),
                    Grades = new List<StudentTranscript.CourseGrade>()
                };
                if (!reader.IsDBNull(reader.GetOrdinal("Grade")))
                {
                    transcript.Grades.Add(new StudentTranscript.CourseGrade
                    {
                        CourseId = reader.GetInt32(reader.GetOrdinal("CourseID")),
                        Title = reader.GetString(reader.GetOrdinal("Title")),
                        Credits = reader.GetInt32(reader.GetOrdinal("Credits")),
                        Grade = reader.GetDecimal(reader.GetOrdinal("Grade")),
                    });
                }
            });

            return transcript;
        }
    }
}
