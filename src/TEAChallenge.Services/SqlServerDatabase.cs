﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace TEAChallenge.Services
{
    public class SqlServerDatabase : Core.IDatabase
    {
        public IDbConnection Connection { get; }

        public bool IsHealthy
        {
            get
            {
                var commandText = "SELECT 42 AS TheAnswerToLifeTheUniverseAndEverything";
                try
                {
                    var answer = ReadScalar<int>(commandText);
                    return (answer == 42);
                }
                catch
                {
                    // TODO: In a real-world scenario it would be good to log
                    // the exception information.
                    return false;
                }
            }
        }

        private readonly string _connectionString;

        public SqlServerDatabase(
            string connectionString)
        {
            _connectionString = connectionString ??
                throw new ArgumentNullException(nameof(connectionString));
        }

        public void Execute(
            string commandText,
            object parameters = null)
        {
            using var conn = new SqlConnection(_connectionString);
            conn.Open();
            var cmd = BuildCommand(conn, commandText, parameters);
            cmd.ExecuteNonQuery();
        }

        public T ReadScalar<T>(
            string commandText,
            object parameters = null)
        {
            using var conn = new SqlConnection(_connectionString);
            conn.Open();
            var cmd = BuildCommand(conn, commandText, parameters);
            return (T)cmd.ExecuteScalar();
        }

        public void ReadAll(
            string commandText,
            object parameters = null,
            Action<IDataReader> action = null)
        {
            using var conn = new SqlConnection(_connectionString);
            conn.Open();
            var cmd = BuildCommand(conn, commandText, parameters);
            using var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                if (action != null)
                {
                    action.Invoke(reader);
                }
            }
        }

        private SqlCommand BuildCommand(
            SqlConnection conn,
            string commandText,
            object parameters = null)
        {
            var cmd = conn.CreateCommand();
            cmd.CommandText = commandText;
            if (parameters != null)
            {
                cmd.Parameters.AddRange(parameters
                    .GetType()
                    .GetProperties()
                    .Select(prop =>
                    {
                        var parameterName = "@" + prop.Name;
                        var parameterValue = prop.GetValue(parameters);
                        if (parameterValue == null)
                        {
                            return new SqlParameter(parameterName, DBNull.Value);
                        }
                        return new SqlParameter(parameterName, parameterValue);
                    })
                    .ToArray());
            }
            return cmd;
        }
    }
}
